#Este programa calcula el area lateral de un cono dadas sus dimensiones
pi=3.141592					#pi es una constante
radio= input("Ingrese radio del cono: ")	#Se ingresan las variables
altura= input("Ingrese altura del cono: ")
radio=float(radio)				#Se convierten a flotante
altura= float(altura)
area= pi*radio*altura				#Se calcula el área
print ("El area lateral del cono es: %f" %area)	#Se imprime por pantalla
