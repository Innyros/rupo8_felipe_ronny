from __future__ import division #funcion en libreria para evitar error en division
from decimal import Decimal     #funcion en libreria para que la division se almacene como decimal
from decimal import getcontext  #funcion en libreria para determinar el largo del decimal
print("\n Ingrese la cantidad de decimales deseadas para \"pi\"")
k=int(input())+2                #aumentamos en 1 la entrada para cumplir con el decimal deseado +1 extra
getcontext().prec = k           #cantidad de decimales
sum=0
for i in range(k):              #serie matematica para calcular pi
    sum += Decimal(pow(16, -i)) * (Decimal(4/(8*i+1)) - Decimal(2/(8*i+4)) - Decimal(1/(8*i+5)) - Decimal(1/(8*i+6)))k=k-2                           #-2 para mostrar en pantalla el valor real de k
print("\nEl valor de \"pi\" con %d decimales es:"%k)
print("%s\b " %sum)             #\b borra el decimal extra para dar un valor exacto de los decimales deseados