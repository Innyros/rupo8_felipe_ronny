numero1= input("Ingrese primer numero: ")	#se ingresan los numeros
numero2= input("Ingrese segundo numero: ")
numero1= int(numero1)				#se convierten a 'int'
numero2= int(numero2)
cuadrado= numero1*numero1			#se calcula el cuadrado

if (cuadrado==numero2):				#se establecen condiciones
	print ("El segundo es el cuadrado exacto del primero")

elif (cuadrado > numero2):
	print ("El segundo es menor que el cuadrado del primero")

else:
	print ("El segundo es mayor que el cuadrado del primero")
